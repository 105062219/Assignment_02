var mainState = {
    preload: function(){
        game.load.image('background', 'assets/background.jpg', 400, 400);
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.audio('backgroundMusic', 'assets/sounds/background.mp3');
        game.load.audio('hurt', 'assets/sounds/hit.wav');
        game.load.audio('bounce', 'assets/sounds/jump.ogg');
    },

    create: function(){
        this.background = game.add.image(-50, 0, 'background');
        this.cursor = game.input.keyboard.createCursorKeys();
        this.player = game.add.sprite(200, 50, 'player');
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.animations.add('left', [0, 1, 2, 3], 19);
        this.player.animations.add('right', [9, 10, 11, 12], 19);
        this.player.animations.add('dropleft', [18, 19, 20, 21], 24);
        this.player.animations.add('dropright', [27, 28, 29, 30], 24);
        this.player.animations.add('dropdown', [36, 37, 38, 39], 24);
        this.player.animations.add('hurt', [8, 17], 25);
        this.backgroundMusic = game.add.audio('backgroundMusic');
        this.player.bounce_sound = game.add.audio('bounce');
        this.player.hurt_sound = game.add.audio('hurt');
        this.backgroundMusic.loop = true; 
        this.backgroundMusic.play();
        this.player.life = 10;
        this.player.unbeatableTime = 0;
        this.player.touchOn = undefined;
        this.ceiling = game.add.image(0, 0, 'ceiling');
        this.leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;
        this.rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;
        game.global.score = 1;
        this.lastTime = 0
        this.platforms = [];
        this.style = {fill: '#FFFFFF', fontSize: '20px'}
        this.text1 = game.add.text(30, 10, '', this.style);
        this.text2 = game.add.text(330, 10, '', this.style);
        game.time.events.loop(Phaser.Timer.SECOND * 1.5, this.score, this);
    },

    score: function(){
        if(this.player.alive == true){
            game.global.score += 1;
        }
    },

    update: function(){
        this.physics.arcade.collide(this.player, this.platforms, this.effects);
        this.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
        this.collide_ceiling();
        if(this.player.life <= 0 || this.player.body.y > 500) {
            this.player.kill();
            this.backgroundMusic.stop();
            this.platforms.forEach(function(s) {s.destroy()});
            this.platforms = [];
            game.state.start('over1');
        }
        this.moveplayer();
        this.platform_moving();
        this.random_platform();
        this.text1.setText('Life: ' + this.player.life);
        this.text2.setText('B ' + game.global.score);
    },

    collide_ceiling: function(){
        if(this.player.body.y < 10) {
            if(this.player.body.velocity.y < 0){
                this.player.body.velocity.y = 0;
            }
            if(game.time.now > this.player.unbeatableTime){
                this.player.life -= 5;
                this.player.hurt_sound.play();
                game.camera.shake(0.01, 500);
                this.player.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    random_platform: function(){
        if(game.time.now > this.lastTime + 600){
            this.lastTime = game.time.now;
            var rand = Math.random() * 100;
            var x = Math.random()*(400 - 96 - 40) + 20;
            var y = 400;
            if(rand < 20){
                this.platform = game.add.sprite(x, y, 'normal');
            } 
            else if (rand < 40){
                this.platform = game.add.sprite(x, y, 'nails');
                game.physics.arcade.enable(this.platform);
                this.platform.body.setSize(96, 15, 0, 15);
            } 
            else if (rand < 50){
                this.platform = game.add.sprite(x, y, 'conveyorLeft');
                this.platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                this.platform.play('scroll');
            } 
            else if (rand < 60){
                this.platform = game.add.sprite(x, y, 'conveyorRight');
                this.platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                this.platform.play('scroll');
            } 
            else if (rand < 80){
                this.platform = game.add.sprite(x, y, 'trampoline');
                this.platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                this.platform.frame = 3;
            } 
            else{
                this.platform = game.add.sprite(x, y, 'fake');
                this.platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            }
            game.physics.arcade.enable(this.platform);
            this.platform.body.immovable = true;
            this.platforms.push(this.platform);
            this.platform.body.checkCollision.down = false;
            this.platform.body.checkCollision.left = false;
            this.platform.body.checkCollision.right = false;
        }
    },

    moveplayer: function(){
        if(this.cursor.left.isDown){
            this.player.body.velocity.x = -150;
        } 
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 150;
        } 
        else{
            this.player.body.velocity.x = 0;
        }
        this.setanimation(this.player);
    },

    setanimation: function(){
        if(this.player.body.velocity.x < 0 && this.player.body.velocity.y > 0) {
            this.player.animations.play('dropleft');
        }
        if(this.player.body.velocity.x > 0 && this.player.body.velocity.y > 0) {
            this.player.animations.play('dropright');
        }
        if(this.player.body.velocity.x < 0 && this.player.body.velocity.y == 0) {
            this.player.animations.play('left');
        }
        if(this.player.body.velocity.x > 0 && this.player.body.velocity.y == 0) {
            this.player.animations.play('right');
        }
        if(this.player.body.velocity.x == 0 && this.player.body.velocity.y != 0) {
            this.player.animations.play('dropdown');
        }
        if(this.player.body.velocity.x == 0 && this.player.body.velocity.y == 0) {
            this.player.frame = 8;
        }
    },

    platform_moving: function(){
        for(var i=0; i < this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },

    effects: function(player, platform){
        if(platform.key == 'conveyorRight'){
            player.body.x += 1.5;
        }
        if(platform.key == 'conveyorLeft'){
            player.body.x -= 1.5;
        }
        if(platform.key == 'trampoline'){
            platform.animations.play('jump');
            player.bounce_sound.play();
            player.body.velocity.y = -300;
        }
        if(platform.key == 'nails'){
            if(player.touchOn !== platform){
                player.life -= 5;
                player.hurt_sound.play();
                player.touchOn = platform;
                game.camera.shake(0.01, 500);
            }
        }
        if(platform.key == 'normal'){
            if(player.touchOn !== platform){
                if(player.life < 10){
                    player.life += 1;
                }
                player.touchOn = platform;
            }
        }
        if(platform.key == 'fake'){
            if(player.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    },
};

var menuState = {
    create: function(){
        var style = {fill: '#FFFFFF', fontSize: '20px'}
        var title = game.add.text(232, 100, '0成本遊戲', style);
        var one = game.add.text(232, 150, '1 player', style);
        var two = game.add.text(232, 200, '2 players', style);
        var scoreLabel = game.add.text(90, 70, '', style);
        var database = firebase.database().ref('/scoreboard').orderByChild('scoreforrank').limitToFirst(5);
        database.once('value', function(snapshot){
            var scoreText = '';
            snapshot.forEach(function(childSnapshot){
                scoreText += childSnapshot.val().name + '  ' + childSnapshot.val().score + '\n';
            });
            scoreLabel.text = scoreText;
        });
        one.inputEnabled = true;
        one.events.onInputUp.add(this.start);
        two.inputEnabled = true;
        two.events.onInputUp.add(this.start_2);
    },

    start: function(){
        game.state.start('main');
    },

    start_2: function(){
        game.state.start('main2');
    }
};

var gameover1 = {
    create: function(){
        this.style = {fill: '#FFFFFF', fontSize: '20px'}
        var player = prompt("Your name", "name");
        if(player != null){
            var database = firebase.database().ref('/scoreboard');
            database.push().set({
                score: game.global.score,
                scoreforrank: -game.global.score,
                name: player
            });
        }
        yourscore = game.add.text(145, 10, 'Your score', this.style);
        score = game.add.text(190, 50, game.global.score, this.style);
        text3 = game.add.text(120, 350, 'Go back to menu', this.style);
        text3.inputEnabled = true;
        text3.events.onInputUp.add(this.end);
    },

    end: function(){
        game.state.start('menu')
    }
}

var mainState2 = {
    preload: function(){
        game.load.image('background', 'assets/background.jpg', 400, 400);
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('player2', 'assets/player2.png', 32, 32);
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.audio('backgroundMusic', 'assets/sounds/background.mp3');
        game.load.audio('hurt', 'assets/sounds/hit.wav');
        game.load.audio('bounce', 'assets/sounds/jump.ogg');
    },

    create: function(){
        this.background = game.add.image(-50, 0, 'background');
        this.cursor = game.input.keyboard.addKeys({
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'a': Phaser.Keyboard.A,
            'd': Phaser.Keyboard.D
        });
        this.player1 = game.add.sprite(100, 50, 'player');
        this.player2 = game.add.sprite(300, 50, 'player2');
        this.playerattr(this.player1);
        this.playerattr(this.player2);      
        this.backgroundMusic = game.add.audio('backgroundMusic');       
        this.backgroundMusic.loop = true; 
        this.backgroundMusic.play();
        this.ceiling = game.add.image(0, 0, 'ceiling');
        this.leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;
        this.rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;
        this.lastTime = 0
        this.platforms = [];
        this.style = {fill: '#FFFFFF', fontSize: '20px'}
        this.text1 = game.add.text(30, 10, '', this.style);
        this.text2 = game.add.text(300, 10, '', this.style);
    },

    playerattr: function(player){
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 19);
        player.animations.add('right', [9, 10, 11, 12], 19);
        player.animations.add('dropleft', [18, 19, 20, 21], 24);
        player.animations.add('dropright', [27, 28, 29, 30], 24);
        player.animations.add('dropdown', [36, 37, 38, 39], 24);
        player.animations.add('hurt', [8, 17], 25);
        player.bounce_sound = game.add.audio('bounce');
        player.hurt_sound = game.add.audio('hurt');
        player.life = 10;
        player.unbeatableTime = 0;
        player.touchOn = undefined;
    },

    update: function(){
        this.physics.arcade.collide([this.player1, this.player2], this.platforms, this.effects);
        this.physics.arcade.collide([this.player1, this.player2], [this.leftWall, this.rightWall]);
        this.physics.arcade.collide(this.player1, this.player2);
        this.collide_ceiling(this.player1);
        this.collide_ceiling(this.player2);
        if(this.player1.life <= 0 || this.player1.body.y > 500) {
            this.player1.kill();
            this.player2.kill();
            this.backgroundMusic.stop();
            this.platforms.forEach(function(s) {s.destroy()});
            this.platforms = [];
            game.state.start('win_2');
        }
        if(this.player2.life <= 0 || this.player2.body.y > 500) {
            this.player1.kill();
            this.player2.kill();
            this.backgroundMusic.stop();
            this.platforms.forEach(function(s) {s.destroy()});
            this.platforms = [];
            game.state.start('win_1');
        }
        this.moveplayer();
        this.platform_moving();
        this.random_platform();
        this.text1.setText('Life: ' + this.player1.life);
        this.text2.setText('Life: ' + this.player2.life);
    },

    end: function(){
        game.state.start('menu')
    },

    collide_ceiling: function(player){
        if(player.body.y < 10) {
            if(player.body.velocity.y < 0){
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime){
                player.life -= 5;
                player.hurt_sound.play();
                game.camera.shake(0.01, 500);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    random_platform: function(){
        if(game.time.now > this.lastTime + 600){
            this.lastTime = game.time.now;
            var rand = Math.random() * 100;
            var x = Math.random()*(400 - 96 - 40) + 20;
            var y = 400;
            if(rand < 20){
                this.platform = game.add.sprite(x, y, 'normal');
            } 
            else if (rand < 40){
                this.platform = game.add.sprite(x, y, 'nails');
                game.physics.arcade.enable(this.platform);
                this.platform.body.setSize(96, 15, 0, 15);
            } 
            else if (rand < 50){
                this.platform = game.add.sprite(x, y, 'conveyorLeft');
                this.platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                this.platform.play('scroll');
            } 
            else if (rand < 60){
                this.platform = game.add.sprite(x, y, 'conveyorRight');
                this.platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                this.platform.play('scroll');
            } 
            else if (rand < 80){
                this.platform = game.add.sprite(x, y, 'trampoline');
                this.platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                this.platform.frame = 3;
            } 
            else{
                this.platform = game.add.sprite(x, y, 'fake');
                this.platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
            }
            game.physics.arcade.enable(this.platform);
            this.platform.body.immovable = true;
            this.platforms.push(this.platform);
            this.platform.body.checkCollision.down = false;
            this.platform.body.checkCollision.left = false;
            this.platform.body.checkCollision.right = false;
        }
    },

    moveplayer: function(){
        if(this.cursor.left.isDown){
            this.player1.body.velocity.x = -150;
        } 
        else if(this.cursor.right.isDown){
            this.player1.body.velocity.x = 150;
        } 
        else{
            this.player1.body.velocity.x = 0;
        }

        if(this.cursor.a.isDown){
            this.player2.body.velocity.x = -150;
        } 
        else if(this.cursor.d.isDown){
            this.player2.body.velocity.x = 150;
        } 
        else{
            this.player2.body.velocity.x = 0;
        }
        this.setanimation(this.player1);
        this.setanimation(this.player2);
    },

    setanimation: function(player){
        if(player.body.velocity.x < 0 && player.body.velocity.y > 0) {
            player.animations.play('dropleft');
        }
        if(player.body.velocity.x > 0 && player.body.velocity.y > 0) {
            player.animations.play('dropright');
        }
        if(player.body.velocity.x < 0 && player.body.velocity.y == 0) {
            player.animations.play('left');
        }
        if(player.body.velocity.x > 0 && player.body.velocity.y == 0) {
            player.animations.play('right');
        }
        if(player.body.velocity.x == 0 && player.body.velocity.y != 0) {
            player.animations.play('dropdown');
        }
        if(player.body.velocity.x == 0 && player.body.velocity.y == 0) {
            player.frame = 8;
        }
    },

    platform_moving: function(){
        for(var i=0; i < this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },

    effects: function(player, platform){
        if(platform.key == 'conveyorRight'){
            player.body.x += 1.5;
        }
        if(platform.key == 'conveyorLeft'){
            player.body.x -= 1.5;
        }
        if(platform.key == 'trampoline'){
            platform.animations.play('jump');
            player.bounce_sound.play();
            player.body.velocity.y = -300;
        }
        if(platform.key == 'nails'){
            if(player.touchOn !== platform){
                player.life -= 5;
                player.hurt_sound.play();
                player.touchOn = platform;
                game.camera.shake(0.01, 500);
            }
        }
        if(platform.key == 'normal'){
            if(player.touchOn !== platform){
                if(player.life < 10){
                    player.life += 1;
                }
                player.touchOn = platform;
            }
        }
        if(platform.key == 'fake'){
            if(player.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    },
};

var one_won = {
    create: function(){
        this.style = {fill: '#FFFFFF', fontSize: '20px'}
        text3 = game.add.text(130, 100, 'Player 1 Won!', this.style);
        text4 = game.add.text(120, 350, 'Go back to menu', this.style);
        text4.inputEnabled = true;
        text4.events.onInputUp.add(this.end);
    },

    end: function(){
        game.state.start('menu');
    }
};

var two_won = {
    create: function(){
        this.style = {fill: '#FFFFFF', fontSize: '20px'}
        text3 = game.add.text(130, 100, 'Player 2 Won!', this.style);
        text4 = game.add.text(120, 350, 'Go back to menu', this.style);
        text4.inputEnabled = true;
        text4.events.onInputUp.add(this.end);
    },

    end: function(){
        game.state.start('menu');
    }
};

var menuState = {
    preload: function(){
        game.load.image('menu_background', 'assets/menu.jpg', 400, 400);
    },

    create: function(){
        this.menu_image = game.add.image(0, 0, 'menu_background');
        var style = {fill: '#FFFFFF', fontSize: '20px'}
        var title = game.add.text(232, 100, 'NS-SHAFT', style);
        var one = game.add.text(232, 150, '1 player', style);
        var two = game.add.text(232, 200, '2 players', style);
        scoreboard = game.add.text(60, 60, 'Scoreboard', style);
        var scoreLabel = game.add.text(70, 120, '', style);
        var database = firebase.database().ref('/scoreboard').orderByChild('scoreforrank').limitToFirst(5);
        database.once('value', function(snapshot){
            var scoreText = '';
            snapshot.forEach(function(childSnapshot){
                scoreText += childSnapshot.val().name + '  ' + childSnapshot.val().score + '\n';
            });
            scoreLabel.text = scoreText;
        });
        one.inputEnabled = true;
        one.events.onInputUp.add(this.start);
        two.inputEnabled = true;
        two.events.onInputUp.add(this.start_2);
    },

    start: function(){
        game.state.start('main');
    },

    start_2: function(){
        game.state.start('main2');
    }
};

var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.add('over1', gameover1);
game.state.add('main2', mainState2);
game.state.add('win_1', one_won);
game.state.add('win_2', two_won);
game.state.add('menu', menuState);
game.state.start('menu');

game.global = {
    score : 0,
}